import React from "react";
import { useState, useEffect } from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import ReactInputMask from "react-input-mask";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import { useNavigate, useParams } from "react-router-dom";
import axios from "axios";
import logo from "../logo.svg";
import "../App.css";

function Register() {
  let navigate = useNavigate();
  let params = useParams();

  const [checkedOne, setCheckedOne] = useState(false);
  const [open, setOpen] = React.useState(false);
  const [open1, setOpen1] = React.useState(false);
  const [data, setData] = useState();

  const [products, setProducts] = useState();

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleClickOpen1 = () => {
    setOpen1(true);
  };

  const handleClose1 = () => {
    setOpen1(false);
  };

  const tel = "[0-9]";
  const char = "[A-Za-z]";
  const char1 = /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/;
  var regex = /[a-zA-Z]+$/;
  const formik = useFormik({
    initialValues: {
      name: "",
      email: "",
      phone: "",
      address: "",
      isAble: false,
      domain: "",
      subjects: [],
      contact_person: "",
      city: "",
      country: "",
    },
    validationSchema: Yup.object({
      name: Yup.string()
        .max(15, "Must be 15 characters or less")
        .required("Required")
        .trim()
        .matches(regex),
      email: Yup.string()
        // .email("invalid Email")
        .required("Required")
        .trim()
        .matches(char1, "It does'nt match the format")
        .max(15, "Must be 15 characters or less"),

      address: Yup.string()
        .max(35, "Must be 35 characters or less")
        .required("Required"),
      phone: Yup.string()
        .required("Tel No. is required")
        .test("len", "Invalid Tel No.", (val) => {
          const val_length_without_dashes = val?.replace(/-|_/g, "").length;
          return val_length_without_dashes === 12;
        }),
      contact_person: Yup.string()
        .max(15, "Must be 15 characters or less")
        .required("Required")
        .trim()
        .matches(regex),
      country: Yup.string()
        .max(15, "Must be 15 characters or less")
        .required("Required")
        .trim()
        .matches(regex),
      city: Yup.string()
        .max(15, "Must be 15 characters or less")
        .required("Required")
        .trim()
        .matches(regex),
    }),
    onSubmit: async (values) => {
      console.log("test");
      if (params.id == "new") {
        const res = await axios.post("http://localhost:8000/api/data", {
          name: values.name,
          email: values.email,
          phone: values.phone,
          city: values.city,
          country: values.country,
          address: values.address,
          contact_person: values.contact_person,
        });
      } else {
        const rep = await axios.get(
          `http://localhost:8000/api/data/${params.id}`
        );
        if (rep && rep.data) {
          console.log(rep.data);
          setData(rep.data);
        }

        const rev = await axios.put(
          `http://localhost:8000/api/data/${params.id}`,
          {
            name: values.name,
            email: values.email,
            phone: values.phone,
            city: values.city,
            country: values.country,
            address: values.address,
            contact_person: values.contact_person,
          }
        );
      }

      navigate(`/Manage`, { replace: true });
    },
  });

  const fetchData = async () => {
    const rep = await axios.get(`http://localhost:8000/api/data/${params.id}`);
    if (rep && rep.data) {
      console.log(rep.data);

      formik.setFieldValue("name", rep.data.name);
      formik.setFieldValue("email", rep.data.email);
      formik.setFieldValue("phone", rep.data.phone);
      formik.setFieldValue("contact_person", rep.data.contact_person);
      formik.setFieldValue("country", rep.data.country);
      formik.setFieldValue("city", rep.data.city);
      formik.setFieldValue("address", rep.data.address);

      setData(rep.data);
    }
  };
  useEffect(() => {
    fetchData();
  }, []);

  const updateOne = () => setCheckedOne(!checkedOne);

  return (
    <form onSubmit={formik.handleSubmit}>
      <div class="flex h-full items-center justify-around bg-blue-200 pt-5 pb-8">
        <div class="w-50 bg-gray-700 rounded-lg">
          <h1 class="my-3 text-center text-2xl text-white">Sign Up</h1>
          <input
            class="m-5 h-12 w-60 border border-black rounded-md pl-1.5"
            type="text"
            name="name"
            placeholder="Enter name"
            onChange={formik.handleChange}
            value={formik.values.name}
          />
          {formik.errors.name ? (
            <p class="text-red-600 m-5 -mt-5">{formik.errors.name}</p>
          ) : null}{" "}
          <br />
          <input
            class="m-5 mt-0 h-12 w-60 border border-black rounded-md pl-1.5"
            type="text"
            name="email"
            placeholder="Enter Email"
            onChange={formik.handleChange}
            value={formik.values.email}
          />
          {formik.errors.email ? (
            <p class="text-red-600 m-5 -mt-5">{formik.errors.email}</p>
          ) : null}{" "}
          <br />
          <ReactInputMask
            class="m-5 mt-0 h-12 w-60 border border-black rounded-md pl-1.5"
            mask="(+051) 99999"
            alwaysShowMask
            type="text"
            name="phone"
            onChange={formik.handleChange}
            value={formik.values.phone}
          />
          {formik.errors.phone ? (
            <p class="text-red-600 m-5 -mt-5">{formik.errors.phone}</p>
          ) : null}{" "}
          <br />
          <input
            class="m-5 h-12 w-60 border -mt-0 border-black rounded-md pl-1.5"
            type="text"
            name="contact_person"
            placeholder="Enter Contact Person"
            onChange={formik.handleChange}
            value={formik.values.contact_person}
          />
          {formik.errors.contact_person ? (
            <p class="text-red-600 m-5 -mt-5">{formik.errors.contact_person}</p>
          ) : null}{" "}
          <br />
          <input
            class="m-5 h-12 w-60 border -mt-0 border-black rounded-md pl-1.5"
            type="text"
            name="city"
            placeholder="Enter City"
            onChange={formik.handleChange}
            value={formik.values.city}
          />
          {formik.errors.city ? (
            <p class="text-red-600 m-5 -mt-5">{formik.errors.city}</p>
          ) : null}{" "}
          <br />
          <input
            class="m-5 h-12 w-60 border -mt-0 border-black rounded-md pl-1.5"
            type="text"
            name="country"
            placeholder="Enter Country"
            onChange={formik.handleChange}
            value={formik.values.country}
          />
          {formik.errors.country ? (
            <p class="text-red-600 m-5 -mt-5">{formik.errors.country}</p>
          ) : null}{" "}
          <br />
          <textarea
            placeholder="Address"
            class="m-5 mb-2 w-60 rounded-md pl-1.5 "
            name="address"
            onChange={formik.handleChange}
            value={formik.values.address}
          ></textarea>
          {formik.errors.address ? (
            <p class="text-red-600 m-5 -mt-4">{formik.errors.address}</p>
          ) : null}{" "}
          <div class="m-5 mt-0 mb-6 flex items-start">
            <div class="flex h-5 items-center">
              <input
                id="check"
                name="isAble"
                type="checkbox"
                value={formik.values.isAble}
                class="focus:ring-3 h-4 w-4 rounded border border-gray-300 bg-gray-50 focus:ring-blue-300 dark:border-gray-600 dark:bg-gray-700 dark:ring-offset-gray-800 dark:focus:ring-blue-600"
                required
                checked={formik.values.isAble}
                onChange={formik.handleChange}
              />
            </div>
            <label
              for="checky"
              class="ml-2 text-sm font-medium text-gray-900 dark:text-gray-300"
            >
              I agree with the{" "}
              <a
                href="#"
                class="text-blue-400 hover:underline dark:text-blue-500"
              >
                term & condition{" "}
              </a>
            </label>
          </div>
          <div class="flex flex-col">
            <button
              type="reset"
              class="m-5 mt-0 w-60 bg-green-400"
              onClick={(e) => handleClickOpen1()}
            >
              Clear
            </button>

            <button
              type="submit"
              id="btn"
              className={`m-5 mt-0 w-60 ${
                formik.values.isAble ? "bg-green-400" : "bg-red-400"
              }`}
              onClick={() => {}}
              disabled={!formik.values.isAble}
            >
              Save
            </button>
          </div>
        </div>

        <div>
          {" "}
          <img src={logo} className="App-logo" alt="logo" />
        </div>

        <Dialog
          open={open}
          onClose={handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">
            <h1>The Details you have entered.</h1>
          </DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              <h1 class="text-blue-600">Your name is {formik.values.name}</h1>
              <h1 class="text-blue-600">Your Email is {formik.values.email}</h1>
              <h1 class="text-blue-600">Your Phone is {formik.values.phone}</h1>
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose}>Okay</Button>
          </DialogActions>
        </Dialog>

        <CustomDailog
          open={open1}
          handleClose={handleClose1}
          agreeAction={() => {
            formik.resetForm();
            handleClose1();
          }}
        />
      </div>

      <div></div>
    </form>
  );
}

export default Register;

function CustomDailog({ open, handleClose, agreeAction }) {
  return (
    <Dialog
      open={open}
      onClose={handleClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">
        <h1>Do You Want To Clear The Form?</h1>
      </DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description"></DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose}>Disagree</Button>
        <Button
          onClick={() => {
            agreeAction();
          }}
        >
          Agree
        </Button>
      </DialogActions>
    </Dialog>
  );
}
