import React from "react";
import { useState, useEffect } from "react";
import Button from "@mui/material/Button";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell, { tableCellClasses } from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TablePagination from "@mui/material/TablePagination";
import TableRow from "@mui/material/TableRow";
import { styled } from "@mui/material/styles";
import axios from "axios";
import EditOutlinedIcon from "@mui/icons-material/EditOutlined";
import AirplanemodeActiveOutlinedIcon from "@mui/icons-material/AirplanemodeActiveOutlined";
import AirplanemodeInactiveOutlinedIcon from "@mui/icons-material/AirplanemodeInactiveOutlined";
import { Link, useNavigate, useParams } from "react-router-dom";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  "&:nth-of-type(odd)": {
    backgroundColor: theme.palette.action.hover,
  },
  // hide last border
  "&:last-child td, &:last-child th": {
    border: 0,
  },
}));

function Managing() {
  let navigate = useNavigate();
  const [data, setData] = useState("");
  const [open, setOpen] = React.useState(null);

  const handleClickOpen = (id) => {
    setOpen(id);
  };

  const handleClose = () => {
    setOpen(null);
  };

  const fetchData = async () => {
    const res = await axios.get(`http://localhost:8000/api/data`);

    if (res && res.data) {
      console.log(res.data);

      setData(res.data);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  const onEdit = async (id) => {
    navigate(`/Register/${id}`, { replace: true });
  };

  const onAdd = async (id) => {
    navigate(`/Register/${id}`, { replace: true });
  };

  return (
    <div>
      <h1 class="text-3xl p-2">Organization Management</h1>
      <Button
        class="flex justify-left p-2 ml-4 w-20 m-2 text-white bg-blue-700 rounded-lg "
        onClick={() => onAdd("new")}
      >
        Add Org
      </Button>
      <br />

      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 700 }} aria-label="customized table">
          <TableHead>
            <TableRow>
              <StyledTableCell>Name</StyledTableCell>
              <StyledTableCell align="right" class="bg-black text-white pl-12">
                Contact
              </StyledTableCell>
              <StyledTableCell align="right" class="bg-black text-white pl-12">
                Email
              </StyledTableCell>
              <StyledTableCell align="right" class="bg-black text-white pl-12">
                City
              </StyledTableCell>
              <StyledTableCell align="right" class="bg-black text-white pl-12">
                Country
              </StyledTableCell>
              <StyledTableCell align="right" class="bg-black text-white pl-12">
                Address
              </StyledTableCell>
              <StyledTableCell align="right" class="bg-black text-white pl-12">
                Action
              </StyledTableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {data &&
              data.map((item) => {
                return (
                  <StyledTableRow key={item.id}>
                    <StyledTableCell component="th" scope="row">
                      {" "}
                      {item.name}
                    </StyledTableCell>
                    <StyledTableCell align="right">
                      {item.phone}
                    </StyledTableCell>
                    <StyledTableCell align="right">
                      {item.email}
                    </StyledTableCell>
                    <StyledTableCell align="right">{item.city}</StyledTableCell>
                    <StyledTableCell align="right">
                      {item.country}
                    </StyledTableCell>
                    <StyledTableCell align="right">
                      {item.address}
                    </StyledTableCell>
                    <StyledTableCell align="right">
                      <Button
                        class="pr-2 -pl-2 text-blue-600"
                        variant="text"
                        type="submit"
                        color="primary"
                        onClick={() => {
                          handleClickOpen(item.id);
                          // onEdit(item.id);
                        }}
                        startIcon={<EditOutlinedIcon />}
                      >
                        {" "}
                      </Button>
                      <Button
                        variant="text"
                        type="submit"
                        color="primary"
                        startIcon={<AirplanemodeActiveOutlinedIcon />}
                      >
                        {" "}
                      </Button>
                    </StyledTableCell>
                  </StyledTableRow>
                );
              })}
          </TableBody>
        </Table>
      </TableContainer>

      <Dialog
        open={open != null ? true : false}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title"></DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            <h1 class="text-2xl">
              Do you really want to edit this organization?
            </h1>
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Disagree</Button>
          <Button
            onClick={() => {
              onEdit(open);
              // fun2();
            }}
            autoFocus
          >
            Agree
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

export default Managing;
