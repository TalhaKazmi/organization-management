import React from "react";
import { useState, useEffect } from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import Managing from "./pages/Managing";

import Register from "./pages/Register";

function App() {
  return (
    <div className="App">
      <Router>
        <Routes>
          <Route path="/Manage" element={<Managing />} />
          <Route path="/Register/:id" element={<Register />} />
        </Routes>
      </Router>
    </div>
  );
}

export default App;
